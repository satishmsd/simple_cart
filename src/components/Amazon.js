import React from 'react'
import list from "../Data"
import Cards from './Card'

export const Amazon = ({handleClick}) => {
    return (
        <section>
            {
                list.map((item) => {
                   return <  Cards item={item} handleClick={handleClick}/>
                })
            }
        </section>
    )
}
