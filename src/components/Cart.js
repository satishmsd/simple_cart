import React, { useEffect, useState } from 'react'
import "../styles/cart.css"
const Cart = ({ cart, setCart, handleChange }) => {
  const [price, setPrice] = useState(0);

  const handlePrice = () => {
    let ans = 0;
    cart.map((item)=>{
      return ans +=item.amount * item.price;
    })
    setPrice(ans)
  }

  const handleRemove = (id)=>{
   const arr = cart.filter((item)=>{
    return item.id !== id
   })
   setCart(arr);
  }


  useEffect(() => {
    handlePrice();
  })

  return (
    <article>
      {
        cart?.map((item) => {
          return (
            <div key={item.id} className='cart_box' >
              <div className='cart_img'>
                <img src={item.img} alt='Cart_img' />
                <p>{item.title}</p>
              </div>
              <div>
                <button onClick={()=>handleChange(item,+1)}>+</button>
                <button>{item.amount}</button>
                <button onClick={()=>handleChange(item,-1)}>-</button>
              </div>
              <div>
                <span>{item.price}</span>
                <button onClick={()=>{handleRemove(item.id)}}> Remove</button>
              </div>
            </div>
          )
        })
      }
      <div className='total'>
        <span>Total Price of your Cart</span>
        <span>Rs - {price}</span>
      </div>
    </article>
  )
}

export default Cart